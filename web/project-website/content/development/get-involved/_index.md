---
title: Get Involved
weight: 2
---

Flamenco is a Free and Open Source project, developed in public under the GPL license.

Join the community on the [#flamenco channel][chat] of Blender Chat do discuss
development topics. New faces are always welcome!

{{< button size="large" relref="/development/getting-started" >}}Get Started Developing Flamenco{{< /button >}}
{{< flamenco/reportBugButton size="large" >}}

If you want to know what kind of work can be done, take a look at the
[workboard][workboard].

[project]: https://projects.blender.org/studio/flamenco
[workboard]: https://projects.blender.org/studio/flamenco/issues
[chat]: https://blender.chat/channel/flamenco
